My name is Michael Veal, I am a am a law clerk in Los Angeles, California. I have a 15 year old son and I have always had a strong love for computers and technology. I am having fun on my college journey as my child heads into high school. I let my hard work in school be a model for his hard work in school.

ITT-310 Bitbucket Link for Milestone 5

https://bitbucket.org/michaelveal/calculator/src/master/

ITT-310 Loom Link for Milestone 5
https://www.loom.com/share/fbc5bbb376584840b107c072b7c03b58